#include <iostream>
#include <ecos.h>
#include <cs.h>

pwork* ECOS_setup(idxint n, idxint m, idxint p, idxint l, idxint ncones, idxint* q,
pfloat* Gpr, idxint* Gjc, idxint* Gir,
pfloat* Apr, idxint* Ajc, idxint* Air,
pfloat* c, pfloat* h, pfloat* b);

idxint ECOS_solve(pwork* w);

void ECOS_cleanup(pwork* w, idxint keepvars);

int main()
{
	pwork* my_work;
	
	idxint n;
	cin>>n;
	my_work = ECOS_setup(n,n,0,1,);


return 0;
}
